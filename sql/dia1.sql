-- Archivos SQL día 1

SELECT * FROM metrobus;

SELECT * FROM metrobus WHERE linea = 'Línea 5';

-- Crear una tabla apartir de una consulta
CREATE TABLE linea5 AS
SELECT * FROM metrobus WHERE linea = 'Línea 5';

-- SQL

CREATE TABLE clientes (
    id serial primary key,
    nombre varchar(255) NOT NULL,
    direccion varchar(255),
    ciudad varchar(255),
    cp int,
    alcaldia varchar(255),
    pais varchar(255)
);

INSERT INTO clientes (
    id,
    nombre,
    direccion,
    ciudad,
    cp,
    alcaldia,
    pais)
VALUES (
    1,
    'Juan Pérez',
    'Calle 21',
    'Ciudad de México',
    15250,
    'V.Carranza',
    'México');

SELECT * FROM clientes;

SELECT * FROM clientes ORDER BY nombre;

SELECT id, nombre, direccion FROM clientes ORDER BY nombre;

SELECT id, nombre, direccion FROM clientes ORDER BY cliente ASC;

SELECT * FROM clientes LIMIT 1;

SELECT COUNT(*) FROM clientes;

SELECT COUNT(nombre) FROM clientes;

SELECT column_name
FROM information_schema.columns
WHERE table_schema = 'public'
AND table_name   = 'clientes'

SELECT nombre FROM clientes
GROUP BY nombre
ORDER BY nombre 

SELECT * FROM coloniascdmx WHERE cve_alc=3;

SELECT * FROM coloniascdmx WHERE cve_alc BETWEEN 3 AND 6;

SELECT * FROM coloniascdmx WHERE nombre='AJUSCO I';

SELECT * FROM coloniascdmx WHERE nombre LIKE 'CENTR%' AND cve_alc=17;

SELECT * FROM coloniascdmx WHERE cve_alc > 10 AND cve_alc < 15;

SELECT * FROM coloniascdmx WHERE nombre In ('CENTRO I', 'MORELOS', 'NARVARTE');


%% INSERT, UPDATE y DELETE

INSERT INTO cliente (nombre, cp) VALUES ('Roberto Jeldrez', 48868);

INSERT INTO cliente VALUES ('Jhonny Aguiar', 39968);

UPDATE mi_tabla SET campo1 = 'nuevo valor campo1' WHERE campo2 = 'N';

DELETE FROM ‘mi_tabla’ WHERE 'columna1' = 'valor1'

SELECT 
	st_area(st_transform(geom, 32614))/10000 as superficie_ha
FROM coloniascdmx 
WHERE nombre='AJUSCO I';