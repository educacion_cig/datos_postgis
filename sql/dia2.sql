Crear una tabla
-- Crear tablas espaciales

-- Puntos
CREATE TABLE punto ( id serial primary key, nombre varchar(50), geom geometry(POINT,4326) );

-- Líneas
CREATE TABLE lineas ( id serial primary key, nombre varchar(25), geom geometry(LINESTRING,4326) );

-- Poligonos

CREATE TABLE poligonos ( 
    id serial primary key, 
    nombre varchar(25), 
    geom geometry(POLYGON,4326) );

-- Insertar registros uno por uno
INSERT INTO puntos (nombre, geom) VALUES ('Oaxaca', ST_GeomFromText('POINT(-94.81 15.78)', 4326));
INSERT INTO puntos (nombre, geom) VALUES ('Michoacan', ST_GeomFromText('POINT(-102.31 19.40)', 4326));
INSERT INTO puntos (nombre, geom) VALUES ('Guerrero', ST_GeomFromText('POINT(-101.10 17.29)', 4326));
INSERT INTO puntos (nombre, geom) VALUES ('Puerto Escondido', ST_GeomFromText('POINT(-97.59 15.10)', 4326));

-- Varios registros
INSERT INTO puntos (nombre, geom) VALUES
 ('Oaxaca', ST_GeomFromText('POINT(-94.81 15.78)', 4326)),
 ('Michoacan', ST_GeomFromText('POINT(-102.31 19.40)', 4326)),
 ('Guerrero', ST_GeomFromText('POINT(-101.10 17.29)', 4326)),
 ('Puerto Escondido', ST_GeomFromText('POINT(-97.59 15.10)', 4326));


-- Crear tablas espaciales

-- Puntos
CREATE TABLE ciudades ( id serial primary key, nombre varchar(50), geom geometry(POINT,4326) );

-- Líneas
CREATE TABLE calles ( id serial primary key, nombre_calle varchar(25), geom geometry(LINESTRING,4326) );

-- Polígonos

CREATE TABLE edificios ( id serial primary key, nombre varchar(25), geom geometry(POLYGON,4326) );


-- Crear índices espaciales

create index puntos_gidx on puntos using gist (geom);
create index lineas_gidx on lineas using gist (geom);
create index poligonos_gidx on poligonos using gist (geom);



-- Otros Ejemplos
-- Polígonos
CREATE TABLE edificios
(
    gid serial,
    nombre character varying(254),
    telefono character varying(254),
    areas numeric,
    perimetro numeric,
    geom geometry(MultiPolygon,
    4326),
	CONSTRAINT edificios_pkey PRIMARY KEY
    (gid)
)
    WITH
(
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE INDEX edificios_geom_idx
ON edificios USING gist
    (geom)
TABLESPACE pg_default;

-- Puntos

CREATE TABLE institutos
    (
        gid serial,
        nombre character,
        telefono character,
        geom geometry(MultiPoint,
        4326),
	CONSTRAINT institutos_pkey PRIMARY KEY
        (gid)
)
        WITH
(
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE INDEX institutos_geom_idx 
    ON edificios 
    USING gist (geom)
TABLESPACE pg_default;

-- Líneas

CREATE TABLE vialidades
(
    gid serial,
    nombre character,
    categoria character,
    geom geometry(LineString,
    4326),
	CONSTRAINT vialidades_pkey PRIMARY KEY
        (gid)
)
WITH
(
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE INDEX vialidades_geom_idx
    ON edificios USING gist
    (geom)
TABLESPACE pg_default;


