-- Consultas espaciales
SELECT nom_mun, geom FROM cdmx_limite WHERE nom_mun = 'Iztapalapa'

-- crear tabla:
create table as
SELECT nom_mun, geom FROM cdmx_limite WHERE nom_mun = 'Iztapalapa'

-- Join Capas
SELECT * FROM cdmx_limite denue

-- seleccionar nombre municipio, geometría y nombre del establecimiento
SELECT c.nom_mun, c.geom. d.nom_estab
FROM cdmx_limite as c, %nombrar a cdmx como c
denue as d %nombrar denue como d
WHERE d.nom_estab LIKE 'BODEGA%' %filtar lo que coincida con bodega

-- intersección
SELECT c.nom_mun, c.geom. d.nom_estab, d.geom
FROM cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE 'BODEGA%'
ST_CONTAINS (d.geom,c.geom) 
-- Todo lo que contenga el polígono, éstas no se unene porque las referencias de cdmx y denue son distintas

-- TRANSFORMAR
SELECT c.nom_mun, c.geom d.nom_estab, d.geom
FROM cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE 'BODEGA%'
ST_CONTAINS (c.geom, ST_TRANSFORM(d.geom, 32614)%Las referencias son las mismas

-- OTRA TRANSFORMADA
SELECT c.nom_mun, c.geom AS c_geom, d.nom_estab, d.geom AS de_geom %AS para hacer referencia de donde cargarlas
FROM cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE 'BODEGA%' AND
ST_CONTAINS (c.geom, ST_TRANSFORM(d.geom, 32614)%Las referencias son las mismas, se llama reproyectar, unión espacial, funciones espaciales ST=SPACIAL TEMPORAL

-- Para contar
SELECT count (*) 
FROM
cdmx_limite

--
SELECT c.nom_mun AS delegacion, c.geom AS c_geom, d.nom_estab, d.geom AS de_geom
FROM
cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE 'BODEGA%' and
st_contains(c.geom, st_transform(d.geom, 32614))

-- Otra consulta
SELECT c.nom_mun, c.geom AS c_geom, d.nom_estab, d.geom AS d_geom
FROM
cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE '7 ELEV%' and
c.nom_mun = 'Iztapalapa'
st_contains(c.geom, st_transform(d.geom, 32614))

%ST_ASTEXT (GEOM) PARA COORDENADAS X, Y
%ST_X (GEOM) PARA COOORDENADA X
%ST_Y (GEOM) PARA COORDENADA Y

-- Otro ejemplo para las coordenadas
select nom_estab, geom, st_astext(geom), st_x(geom), st_y(geom) from denue
limit 3

-- Intersect
SELECT c.nom_mun, c.geom AS c_geom, d.nom_estab, d.geom AS de_geom
FROM
cdmx_limite as c,
denue as d 
WHERE d.nom_estab LIKE '7 ELEV%' and
c.nom_mun = 'Iztapalapa'
st_intersects(c.geom, st_transform(d.geom, 32614)) %Para polígonos

-- Otra búsqueda con contains, g
select d.tipo_vial, d.geom, c.geom
from
denue as d,
cdmx_limite as c
where
st_contains(c.geom, st_transform(d.geom, 32614))
group by d.tipo_vial
order by d.tipo_vial asc



-- Consultas en función de preguntas

-- ¿Cual es la longitud total de todas las vialidades de la CDMX expresadas en kilómetros?

select sum(ST_Length(geom))/1000 AS vialudades from calle_df;

select sum(ST_Length(ST_Transform(geom, 900913)))/1000 AS vialidades from mex_eje_vial;

-- ¿Cual es la longitud total de la calle Insurgentes de la CDMX expresadas en kilómetros?

select sum(ST_Length(geom))/1000 AS vialidades from calles_df
WHERE nombre = 'INSURGENTES' AND AND vialidad = 'Vialidad Primaria'
;

-- ¿Cual es la superficie de la alcaldía Miguel Hidalgo en hectáreas?

SELECT
  ST_Area(geom)/10000 AS hectarias
FROM cdmx
WHERE nom_mun = 'Miguel Hidalgo';

SELECT
ST_Area(ST_Transform(geom, 900913))/10000 AS hectarias
FROM mex_municipio
WHERE nomgeo = 'Miguel Hidalgo';

SELECT
ST_Area(ST_Transform(geom, 900913))/1000000 AS kilometros2
FROM mex_municipio
WHERE nomgeo = 'Miguel Hidalgo';

-- ¿Cual es el alcaldía con mayor superficie del CDMX?

SELECT
  nomgeo,
  ST_Area(geom)/10000 AS hectarias
FROM
  mex_municipio
ORDER BY hectarias DESC
LIMIT 1;

SELECT
  nomgeo,
  ST_Area(geom)/1000000 AS kilometros2
FROM
  mex_municipio
ORDER BY hectarias DESC
LIMIT 1;

-- ¿Cuál es la longitud de las calles contenidas por completo dentro de cada alcaldía?

-- Este es un ejemplo de "unión espacial", ya que estamos utilizando datos de dos tablas (haciendo una unión) pero utilizando una condición de interacción espacial ("contained") como la condición de unión en lugar del enfoque relacional habitual de unión de la clave primaria:

SELECT
  m.nom_mun,
  sum(ST_Length(r.geom))/1000 as vialidades_km
FROM
  mex_eje_vial AS r,
  mex_municipio AS m
WHERE
  ST_Contains(m.geom,r.geom)
GROUP BY m.nom_mun
ORDER BY vialidades_km;

-- ¿Cuál es número total de calles contenidas por completo dentro de cada alcaldía?

SELECT
  m.nom_mun,
  count(ST_Length(r.geom)) as numero_calles_total
FROM
  calles_df AS r,
  cdmx AS m
WHERE
  ST_Contains(m.geom,r.geom)
GROUP BY m.nom_mun
ORDER BY numero_calles_total DESC;

-- Crear una tabla con todas las carreteras de la alcaldía Iztapalapa

-- Este es un ejemplo de "superposición", que tomo dos tablas y extrae una tabla nueva que contiene un resultado de un recorte espacial. A diferencia de la "Unión espacial" del ejemplo anterior, esta consulta en realidad crea nuevas geometrías. Una superposición es como una unión espacial "turbo-cargada", y es útil para un trabajo de análisis mas exacto:

SELECT
  ST_Intersection(r.geom, m.geom) AS interseccion_geom,
  ST_Length(r.geom) AS vial_orig_length,
  r.*
FROM
  mex_eje_vial AS r,
  mex_municipio AS m
WHERE  m.nomgeo = 'Toluca' AND ST_Intersects(r.geom, m.geom);


-- ¿Cual es la longitud en kilómetros de "Tollocán" en Toluca?

SELECT
  sum(ST_Length(r.geom))/1000 AS kilometros
FROM
  mex_eje_vial r,
  mex_municipio m
WHERE  r.nomvial = '20 de Noviembre' AND m.nomgeo = 'Toluca'
        AND ST_Contains(m.geom, r.geom) ;

-- ¿Cual es el polígono de municipios mas grande que tiene un agujero?

SELECT gid, nomgeo, ST_Area(geom) AS area
FROM mex_municipio
WHERE ST_NRings(geom) > 1
ORDER BY area DESC LIMIT 1;

-- Los nombres de los alcaldías por los que cruza la calle 'Insurgentes'

SELECT m.nom_mun
FROM cdmx m JOIN calles_df r
ON ST_Crosses(m.geom, r.geom)
WHERE r.nombre = 'INSURGENTES' AND vialidad = 'Vialidad Primaria';


-- ¿Cuál es la distancia que existe entre Toluca y la Ciudad de México?

select st_distance(t.geom, cdmx.geom) as distance
from ciudades_mundo t, ciudades_mundo cdmx
where t.name = 'Toluca' and cdmx.name = 'Mexico City' and cdmx.iso_a2 = 'MX'

-- En KM

select st_distance(st_transform(t.geom, 900913), st_transform(cdmx.geom, 900913))/1000 as distance
from ciudades_mundo t, ciudades_mundo cdmx
where t.name = 'Toluca' and cdmx.name = 'Mexico City' and cdmx.iso_a2 = 'MX'

-- El problema es que no se puede usar un sistema de coordenadas proyectadas para obtener distancias sobre una esfera. Es necesario tener en cuenta la curvatura terrestre. Para eso, podemos usar el tipo geography. Al llamar a st_distance sobre datos de tipo geography, se usa la esfera para hacer cálculos, y se devuelve el resultado en metros.

-- La consulta correcta sería:

select st_distance(geography(o.geom), geography(d.geom))/1000 as distance
from ciudades_mundo o, ciudades_mundo d
where o.name = 'Toluca' and d.name = 'Mexico City' and d.iso_a2 = 'MX'

-- El numero de escuelas que hay en cada uno de los municipios del Estado de México:

select m.nomgeo, count(p.nombre_act) as escuelas from mex_municipio m join
denue_inegi_15_ p on st_contains(m.geom, p.geom) where p.nombre_act like '%scuelas%'
group by m.nomgeo order by escuelas desc

select st_distance(st_transform(o.geom, 900913), st_transform(d.geom, 900913))/1000 as distancia_plana,
st_distance(geography(o.geom), geography(d.geom))/1000 as distancia_esfera
from ciudades_mundo o, ciudades_mundo d
where o.name = 'Toluca' and d.name = 'Mexico City' and d.iso_a2 = 'MX'

-- ¿Cuál es la superficie del Estado de México?

ST_Transform(geom, 900913))/1000000 from mex_entidad where nomgeo = 'México'

-- Agrega el área o superficie del Estado de México en  el campo área de la tabla mex_entidad

-- Agregar el campo

ALTER TABLE mex_entidad ADD COLUMN area real;

update mex_entidad set area=(
select ST_Area(ST_Transform(geom, 900913))/1000000 from mex_entidad where nomgeo = 'México'
)

-- ¿Cuál es el área de influencia en 10 metros sobre la calle Insurgentes ?
-- Buffer

CREATE TABLE buffer_insurgentes_luis AS
SELECT
  ST_Buffer(geom, 10, 'endcap=round join=round')
  FROM calles_df WHERE nombre = 'INSURGENTES' 
   AND vialidad = 'Vialidad Primaria'
;


-- Consulta para crear
-- una tabla espacial
-- usando las coordenadas

select
    id, linea, nombre, lat, lon,
    ST_GeomFromText('POINT(-99.7386947 19.2721924)',4326) as geom1
FROM metrobus